#include <main.h>

void setup() {
  //SERIAL COMMUNICATION
  Serial.begin(115200);
  Serial.println("Starting Etch-a-sketch...");

  // PINS SETUP
  for (int i = 0; i < numBut;i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }
  pinMode(ledPin, OUTPUT);

  //WIFI
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid); // SSID = name of the network you are connecting to.
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  WiFi.setSleep(false);

  while (WiFi .status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");     
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  digitalWrite(ledPin, HIGH); //turn on the LED when we're connected to wifi.
}

void loop() {
  checkPots();
  checkButtons();

  delay(16);
}

void checkPots() {
  for (int i=0; i<numPot; i++){
        int potValue = analogRead(pots[i]);

        // construct/send OSC message
        OSCMessage msg("/pots/p");
        msg.add(i);
        msg.add(potValue);
        Udp.beginPacket(outIp, outPort);
        msg.send(Udp);
        Udp.endPacket();
        msg.empty();
  }
}

void checkButtons() {
  // arrays to hold current and last state of buttons 
  static byte lastState[numBut];
  static byte currentState[numBut];

  for (int i=0; i<numBut; i++){
    // read the button pins and fill current array
    currentState[i] = digitalRead(buttons[i]);
    
    // compare each index of the current and last array
    if (currentState[i] != lastState[i]){  //if they are not the same as last time     
        // construct/send OSC message
        if(currentState[i] == 0) {
          OSCMessage msg("/buttons/b");
          msg.add(i);
          msg.add(1);
          Udp.beginPacket(outIp, outPort);
          msg.send(Udp);
          Udp.endPacket();
          msg.empty();
        }
        
        // overwrite the last state with the current one
        lastState[i] = currentState[i];
    }
  }
}