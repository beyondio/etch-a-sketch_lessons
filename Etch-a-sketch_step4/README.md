# Digital Etch-a-Sketch!
## Step 4: Introducing the OSC protocol
_Adding the [OSC library](https://github.com/CNMAT/OSC/) and making first contact with our drawing application: Etch-a-Sketch.app_

### Microcontroller software
Adjust the `IPAddress` variable in `main.h` to your computers IP address.
To find out your systems IP address: 
- Go to System Preferences > Network. 
- Select Wi-Fi
- When connected, your IP address should be displayed underneath your connection status.

_Save & upload the project to the Huzzah32_

Serial monitor shows the same values as step 3.

### **Etch-a-Sketch.app**
Connection between microcontroller and this application is implemented in **step 4 and final**.

To debug OSC signals:
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `show/hide incoming OSC`

To use accelerometer (only works in the final step):
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `Toggle accelerometer`
- This activates the accelerometer as an XY controller.

Additionally, shake your breadboard to clear your drawing. Just like a real Etch-a-Sketch!