# Digital Etch-a-Sketch!
## Final: Adding a touch of magic, aka **the accelerometer**

### Hardware connection

![Breadboard overview](breadboard.png)
<sup>_Adding the LIS3DH Triple Axis sensor_</sup>

### Microcontroller software
Make sure you adjust the `IPAddress` variable again to the same value as step 4.

_Save & upload the project to the Huzzah32_

### **Etch-a-Sketch.app**
Connection between microcontroller and this application is implemented in **step 4 and final**.

To debug OSC signals:
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `show/hide incoming OSC`

To use accelerometer (only works in the final step):
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `Toggle accelerometer`
- This activates the accelerometer as an XY controller.

Additionally, shake your breadboard to clear your drawing. Just like a real Etch-a-Sketch!