# Digital Etch-a-Sketch!
## Physical Computing workshop

_The idea for this workshop was to build a digital version of the [etch-a-sketch](https://images-na.ssl-images-amazon.com/images/I/71U0trs1aNL._SL1500_.jpg), using a wifi-capable microcontroller and some basic components to control a custom drawing application over OSC ([OpenSoundcontrol](http://opensoundcontrol.org/))._

![Example of result](https://beyond.io/projects/etch-a-sketch/workshop_result.jpg)
<sup>_Drawing example_</sup>

![Finished breadboard](https://beyond.io/projects/etch-a-sketch/workshop_breadboard.jpg)
<sup>_Finished breadboard_</sup>



### **Hardware**
- Microcontroller: [Adafruit Huzzah32](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/overview) based on ESP32
- Tripple Axis Accelerometer: [Adafruit LIS3DH breakout](https://learn.adafruit.com/adafruit-lis3dh-triple-axis-accelerometer-breakout)
- 4 Potentiometers + 3 momentary switches

### **Microcontroller software**

Software is written with [Visual Studio Code](https://code.visualstudio.com/) with [platformio](https://platformio.org/) extension using the Arduino IDE and split up in the following steps:

1. **Basics:**
Connecting a single button and potentiometer and checking their values in the serial monitor.

2. **More buttons:**
Adding more pots and buttons and introducing the concept of the `for loop`

3. **Adding wifi connectivity**
4. **Introducing the OSC protocol**
5. **Final step: Adding the accelerometer**

### **Etch-a-Sketch.app**
Connection between microcontroller and this application is implemented in **step 4 and final**.

To debug OSC signals:
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `show/hide incoming OSC`

To use accelerometer (only works in the final step):
- run Etch-a-Sketch.app
- press `d`
- click the toggle next to `Toggle accelerometer`
- This activates the accelerometer as an XY controller.

Additionally, shake your breadboard to clear your drawing. Just like a real Etch-a-Sketch!