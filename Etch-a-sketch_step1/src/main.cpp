#include <main.h>

void setup() {
  //SERIAL COMMUNICATION
  Serial.begin(115200);
  Serial.println("Starting Etch-a-sketch...");

  // initialise your pins, make sure they know what they're supposed to do.
  pinMode(button, INPUT_PULLUP); // PULLUP > pin is set to 1,  
  pinMode(ledPin, OUTPUT);
}

void loop() {
  int potValue = analogRead(pot);
  Serial.print("potentiometer value:");
  Serial.print(potValue);
  Serial.println();

  int buttonValue = digitalRead(button);

  digitalWrite(ledPin, !buttonValue);

  delay(1000); // wait for 1sec (1000ms) and continue.
}