# Digital Etch-a-Sketch!
## Step 1: Basics
_Connecting a single button and potentiometer and checking their values in the serial monitor._

### Hardware connection

![Breadboard overview](breadboard.png)

### Microcontroller software
_Upload the project to the Huzzah32_

The potentiometer value should be visible in the serial monitor.
Pressing the momentary switch turns on the red LED on pin #13 (top left on the Huzzah32).