# Digital Etch-a-Sketch!
## Step 3: Adding wifi connectivity
_Adding the WiFi library and making sure we manage to connect to our access point_

### Microcontroller software
- Adjust `ssid` and `pass` variables in `main.h`.
- Save & Upload the project to the Huzzah32
- Wifi connection status gets displayed in the serial monitor.