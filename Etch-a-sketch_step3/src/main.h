// LIBS
// ====================================================
#include <Arduino.h>
#include <WiFi.h>

// METHODS
// ====================================================
void checkPots(); //function/method to check the potentiometers.
void checkButtons(); //function/method to check the buttons.

byte ledPin = 13;

//button pin array, add the pins your buttons are on
byte buttons[] = {15,32,14};
const int numBut = sizeof(buttons);

//fader pin array, add the pins your sensors are on
byte pots[] = {A2,A3,A4,A9};
const int numPot = sizeof(pots);

// WIFI & OSC
// ====================================================
char ssid[] = "imagining_code"; // your network SSID (name)
char pass[] = "codecode"; // your network password

WiFiUDP Udp; // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 0, 99); // CHANGE TO IP OF TARGET COMPUTER
const unsigned int outPort = 8000; // remote port to receive OSC