#include <main.h>

void setup() {
  //SERIAL COMMUNICATION
  Serial.begin(115200);
  Serial.println("Starting Etch-a-sketch...");

  // PINS SETUP
  for (int i = 0; i < numBut;i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }
  pinMode(ledPin, OUTPUT);
}

void loop() {
  checkPots();
  checkButtons();

  delay(1000); // delay code execution for readability.
}

void checkPots() {
  for (int i=0; i<numPot; i++){
    int potValue = analogRead(pots[i]);
    Serial.print("potentiometer "); // print puts everything on the current line.
    Serial.print(i);
    Serial.print(": ");
    Serial.print(potValue); 
    Serial.println(); // println = "print line" moves to the next line.
  }
  Serial.println(); // extra blank line for readability.
}

void checkButtons() {
  //arrays to hold current and last state of buttons 
  static byte lastState[numBut];
  static byte currentState[numBut];

  //start a for loop
  for (int i=0; i<numBut; i++){
    // read the button pins and fill current array
    currentState[i] = digitalRead(buttons[i]);
    
    //compare each index of the current and last array
    if (currentState[i] != lastState[i]){  //if they are not the same as last time
        Serial.print("button ");
        Serial.print(i);
        Serial.print(": ");
        Serial.print(currentState[i]);
        Serial.println();
        
        digitalWrite(ledPin, !currentState[i]);

        //overwrite the last state with the current one
        lastState[i] = currentState[i];
    }
  }
  Serial.println();
}