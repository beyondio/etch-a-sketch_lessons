#include <Arduino.h>

void checkPots(); //function/method to check the potentiometers.
void checkButtons(); //function/method to check the buttons.

byte ledPin = 13;

//button pin array, add the pins your buttons are on
byte buttons[] = {15,32,14};
const int numBut = sizeof(buttons);

//fader pin array, add the pins your sensors are on
byte pots[] = {A2,A3,A4,A9};
const int numPot = sizeof(pots);