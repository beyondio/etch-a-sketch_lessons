# Digital Etch-a-Sketch!
## Step 2: More buttons
_Adding more pots and buttons and introducing the concept of the ```for loop```_

### Hardware connection

![Breadboard overview](breadboard.png)

### Microcontroller software
_Upload the project to the Huzzah32_

The potentiometer values should be visible in the serial monitor.
Pressing the momentary switches turns on the red LED on pin #13 (top left on the Huzzah32).